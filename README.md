# Dotfiles

> Herbstluftwm
![Example Scrot](/screenshots/x11.png)

> Hyprland
![Hyprland](/screenshots/wayland.png)

These dotfiles are managed with [Chezmoi](https://www.chezmoi.io/)

## Info
- OS: [Arch Linux](https://archlinux.org)
- Window Manager: [herbstluftwm](https://github.com/herbstluftwm/herbstluftwm)/[hyprland](https://hyprland.org/)
- Terminal: [kitty](https://github.com/kovidgoyal/kitty)
- Shell: [zsh](https://www.zsh.org/)
- Browser: [qutebrowser](https://github.com/qutebrowser/qutebrowser)/[brave](https://brave.com/linux/)
- Bar: [polybar](https://github.com/polybar/polybar)/[waybar](https://github.com/Alexays/Waybar)
- Music: [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp) with [mpd](https://github.com/MusicPlayerDaemon/mpd)
- File manager: [lf](https://github.com/gokcehan/lf)
- Editor: [neovim](https://github.com/neovim/neovim)
- Tasks: [taskwarrior](https://github.com/GothenburgBitFactory/taskwaqrrior) with [vit](https://github.com/vit-project/vit)
- RSS feeds: [newsboat](https://github.com/newsboat/newsboat)
- Color scheme: [alduin](https://github.com/AlessandroYorba/Alduin)/[decay](https://github.com/decaycs)

## Install

1. `chezmoi init https://gitlab.com/radiumz/dotfiles`
2. `chezmoi apply -R`

## Credits
- [barbaross](https://gitlab.com/barbaross/muspelheim)
- [pyratebeard](https://gitlab.com/pyratebeard/dotfiles)
- [JLErvin](https://github.com/JLErvin/dotfiles)
- [6gk](https://github.com/6gk/polka)
