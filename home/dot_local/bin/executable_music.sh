#!/bin/bash

session_name="Music"

# Check if the session already exists
if tmux has-session -t "$session_name" 2>/dev/null; then
	# Session exists, attach to it
	kitty -1 -e tmux attach-session -t "$session_name"
else
	# Session doesn't exist, create a new one
	kitty -1 -e tmux new-session -s "$session_name" "tmux source-file ~/.config/ncmpcpp/tmux_session"
fi
