#!/bin/sh

hc() {
	herbstclient "$@"
}

# rules
hc unrule -F
hc rule focus=on # normally focus new clients
hc rule instance=dropdown floating=on focus=on
hc rule instance=qute floating=on floatplacement=center focus=on
hc rule class=Nsxiv floating=on floatplacement=center focus=on
hc rule class=imv floating=on floatplacement=center focus=on
hc rule class=mpv floating=on floatplacement=center focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' pseudotile=on
hc rule windowtype='_NET_WM_WINDOW_TYPE_DIALOG' focus=on floatplacement=center
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)' manage=off
hc rule class=firefox tag=1
hc rule title=Neomutt tag=2
hc rule title=Vit tag=2
hc rule title=Khal tag=2
hc rule class=Zathura tag=4
hc rule title=Amfora tag=5
hc rule title=Phetch tag=5
hc rule title=Newsboat tag=5
hc rule title=Tuir tag=5
hc rule title=WeeChat tag=6
hc rule class=Music tag=7 #fix me
hc rule class=Steam tag=8 floating=on floatplacement=center
