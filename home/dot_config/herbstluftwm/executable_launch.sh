#!/bin/sh

launch() {
	local running
	running="$(pgrep -x "$1")"
	[ "$running" ] && kill "$running" 2>/dev/null
	eval "$*" >/dev/null 2>&1 &
	echo -e "$(tput setf 1)INFO\e[0m: Restarted $1."
}

xset m 0 0 &
exec ~/.config/polybar/launch.sh &
launch ~/.config/ncmpcpp/idle.sh
exec xautolock -time 5 -locker 'lock' -killtime 10 -killer 'systemctl suspend' -notify 120 -notifier dim-screen.sh -detectsleep &
setxkbmap -option "caps:escape" &
launch /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
launch ~/.config/herbstluftwm/idle.sh
launch xbanish -s -t 2
launch xob_start
launch playerctld
launch mpDris2
launch ~/.fehbg
launch picom
