#!/bin/sh

hc() {
	herbstclient "$@"
}

# theme
hc set hide_covered_windows false
hc set frame_border_active_color '#af875f'
hc set frame_border_normal_color '#1c1c1c'
hc set frame_border_inner_color '#424242'
hc set frame_border_inner_width 5
hc set frame_border_width 8
hc set show_frame_decorations focused_if_multiple
hc set frame_bg_transparent 1
hc set frame_transparent_width 0
hc set frame_gap 12
hc set frame_padding 0
hc set focus_follows_mouse 1
hc set snap_gap 12

hc attr theme.reset 1

hc attr theme.title_when 'multiple_tabs'
hc attr theme.title_align 'center'
hc attr theme.title_color '#dfdfaf'
hc attr theme.title_font 'monospace'
hc attr theme.title_height 40
hc attr theme.title_depth 31
hc attr theme.border_width 0
#hc attr theme.inner_width 5
#hc attr theme.outer_width 3
hc attr theme.color '#1c1c1c'
hc attr theme.tab_color '#424242'
hc attr theme.inner_color '#424242'
hc attr theme.normal.outer_color '#1c1c1c'
hc attr theme.active.outer_color '#af875f'
hc attr theme.urgent.outer_color '#af5f5f'
hc attr theme.floating.border_width 8
hc attr theme.floating.inner_width 5
hc attr theme.floating.outer_width 3

hc set window_gap 0
hc set smart_window_surroundings 0
hc set smart_frame_surroundings off
hc set focus_stealing_prevention true
hc set mouse_recenter_gap 0
hc set focus_crosses_monitor_boundaries 1
hc set swap_monitors_to_get_tag 0
hc set raise_on_focus 1
