# Basic Settings
set hidden true
set ignorecase true
set icons true
set previewer ~/.local/bin/preview
set shell sh

# Custom Functions
cmd mkdir ${{
  printf "Directory Name: "
  read ans
  mkdir $ans
}}

cmd mkfile ${{
  printf "File Name: "
  read ans
  $EDITOR $ans
}}

cmd setwallpaper %cp "$f" ~/.config/wall.jpg && xwallpaper --zoom "$f"

cmd trash ${{
  trash-put "$f"
}}

# dedicated keys for file opener actions
map o &mimeopen $f
map O $mimeopen --ask $f

# define a custom 'open' command
# This command is called when current file is not a directory. You may want to
# use either file extensions and/or mime types here. Below uses an editor for
# text files and a file opener for the rest.
cmd open ${{
    case $(file --mime-type $f -b) in
        text/*) nvim $fx;;
        *) for f in $fx; do setsid $OPENER $f > /dev/null 2> /dev/null & done;;
    esac
}}

# Drag and drop
cmd dragon %dragon-drop -a -x $fx
cmd dragon-stay %dragon-drop -a $fx
cmd dragon-individual %dragon-drop $fx
cmd cpdragon %cpdragon
cmd mvdragon %mvdragon
cmd dlfile %dlfile

# Archive bindings
cmd unarchive ${{
  case "$f" in
      *.zip) unzip "$f" ;;
      *.tar.gz) tar -xzvf "$f" ;;
      *.tar.bz2) tar -xjvf "$f" ;;
      *.tar) tar -xvf "$f" ;;
      *) echo "Unsupported format" ;;
  esac
}}

# Trash bindings
cmd trash ${{
  files=$(printf "$fx" | tr '\n' ';')
  while [ "$files" ]; do
    file=${files%%;*}

    trash-put "$(basename "$file")"
    if [ "$files" = "$file" ]; then
      files=''
    else
      files="${files#*;}"
    fi
  done
}}

cmd restore_trash ${{
  trash-restore
}}

# Bindings
# Remove some defaults
map m
map o
map n
map "'"
map '"'
map d

map c $vscodium "$f"

map au unarchive

# Basic Functions
map . set hidden!
map DD trash
map p paste
map x cut
map y copy
map <enter> open
map R reload
map mf mkfile
map md mkdir
map bg setwallpaper
map C clear

map dd trash
map du restore_trash

map ee $$EDITOR "$f"


# Dragon Mapping
map dr dragon
map ds dragon-stay
map di dragon-individual
map dm mvdragon
map dc cpdragon
map dl dlfile


# Movement
map gd cd ~/Documents
map gD cd ~/Downloads
map gp cd ~/Pictures
map gc cd ~/.config
map gr cd ~/Code
map gs cd ~/.local/bin
map gt cd ~/.local/share/Trash/files
