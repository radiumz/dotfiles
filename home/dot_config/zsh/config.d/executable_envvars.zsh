export TERM='xterm-256color'
export EDITOR='nvim'
export VISUAL='nvim'
export SUDO_PROMPT=$'Password for ->\033[32;05;16m %u\033[0m  '
command_not_found_handler() {
  printf 'Command not found ->\033[32;05;16m %s\033[0m \n' "$0" >&2
  return 127
}
# export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#4d4d4d"
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'  --color fg:7,bg:-1,hl:6,fg+:6,bg+:-1,hl+:6 --color info:2,prompt:1,spinner:5,pointer:5,marker:3,header:8'

export FZF_DEFAULT_COMMAND="fd . --max-depth=1 --hidden"

