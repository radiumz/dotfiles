export PATH=$HOME/.config/tmux/plugins/t-smart-tmux-session-manager/bin:$PATH
export PATH=$HOME/.config/tmux/plugins/tmuxifier/bin:$PATH
export PATH=$HOME/.local/share/cargo/bin:$PATH
export PATH=$HOME/.bling:$PATH
export PATH=$HOME/.config/emacs/bin:$PATH
