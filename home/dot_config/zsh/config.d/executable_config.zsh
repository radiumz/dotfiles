zstyle ':fzf-tab:complete:*:*' fzf-preview 'preview $realpath'
zstyle ':fzf-tab:*' switch-group ',' '.'
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':fzf-tab:*' fzf-min-height 100

case "$TERM" in (rxvt|rxvt-*|st|st-*|*xterm*|(dt|k|E)term)
    local term_title () { print -n "\e]0;${(j: :q)@}\a" }
    precmd () {
      local DIR="$(print -P '%c')"
      term_title "$DIR" "zsh"
    }
    preexec () {
      local DIR="$(print -P '%c')"
      local CMD="${(j:\n:)${(f)1}}"
      term_title "$CMD"
    }
  ;;
esac

umask 022
zmodload zsh/zle
zmodload zsh/zpty
zmodload zsh/complist

autoload _____vi_search_fix
autoload -Uz colors
autoload -U compinit
colors

zle -N _____vi_search_fix
zle -N _____sudo_command_line
zle -N _____toggle_right_prompt
zle -N _____toggle_left_prompt

zstyle ':completion:*:git-checkout:*' sort false
zstyle ':completion:*:descriptions' format '[%d]'
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':fzf-tab:complete:*:*' fzf-preview 'preview $realpath'
zstyle ':fzf-tab:*' switch-group ',' '.'
zstyle ':fzf-tab:*' fzf-min-height 100

HISTFILE="$HOME/.local/share/zsh/.zhistory"
HISTORY_IGNORE="(echo *|printf *|print *|ls|fm)"
HISTSIZE=10000
SAVEHIST=10000

ZSH_AUTOSUGGEST_USE_ASYNC="true"
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor regexp root line)
ZSH_HIGHLIGHT_MAXLENGTH=512
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#4d4d4d"

while read -r option
do 
  setopt $option
done <<-EOF
AUTOCD
AUTO_MENU
PUSHDSILENT
AUTOPUSHD
PUSHDMINUS
AUTO_PARAM_SLASH
COMPLETE_IN_WORD
NO_MENU_COMPLETE
HASH_LIST_ALL
ALWAYS_TO_END
NOTIFY
NOHUP
MAILWARN
INTERACTIVE_COMMENTS
NOBEEP
APPEND_HISTORY
SHARE_HISTORY
INC_APPEND_HISTORY
EXTENDED_HISTORY
HIST_IGNORE_ALL_DUPS
HIST_IGNORE_SPACE
HIST_NO_FUNCTIONS
HIST_EXPIRE_DUPS_FIRST
HIST_SAVE_NO_DUPS
HIST_REDUCE_BLANKS
EOF

while read -r option
do
  unsetopt $option
done <<-EOF
FLOWCONTROL
NOMATCH
CORRECT
EQUALS
EOF

# vi mode
bindkey -v
export KEYTIMEOUT=1

