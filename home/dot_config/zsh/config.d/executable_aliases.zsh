alias cm='chezmoi'
alias cma='chezmoi add'
alias cmap='chezmoi apply'
alias cmcd='cd $(chezmoi source-path)'
alias cmd='chezmoi diff'
alias cme='chezmoi edit'
alias cmm='chezmoi merge'
alias cmrm='chezmoi remove'
alias cmst='chezmoi status'
alias cmup='chezmoi update'
alias mast="gpg --home='/run/media/daniel/New Volume/.gnupg'"
alias pi='sudo pacman -S'
alias pu='sudo pacman -Syu'
alias ra='ranger'

alias c='cd $(fd --type d . | fzf --preview "exa --long --icons --color=always {}")'
alias .c='cd $(fd --type d --hidden . | fzf --preview "exa --long --icons --color=always {}")'
alias lssh='ls --hyperlink=auto'

alias fff='fc-list : family | awk -F"," "{print $1}" | sort | uniq | fzf | tr -d "\n" | xclip'

# zellij
alias z='zellij'

alias encrypt='gpg --armor --recipient aspectsidesxyz@gmail.com --encrypt'

alias ts='task'
alias tss='task summary'
alias tsa='task add'
alias tsdd='task delete'
alias tsd='task done'
alias tsm='task modify'
alias xr="sudo xbps-remove -Ro"
alias xc="sudo xbps-remove -Oo"
alias xu='sudo xbps-install -Su'
alias xl='xbps-query -l'
alias xf='xbps-query -f'
alias xd='xbps-query -x'
alias xm='xbps-query -m'
 

alias v="nvim"
alias rm="trash-put"
alias gl='git clone'
alias ttc='tty-clock -c -C 7 -r -f "%A, %B %d"'
alias svim='sudoedit'
alias wgu="doas wg-quick up winterborne"
alias wgd="doas wg-quick down winterborne"
alias m='neomutt'
alias r='newsboat' 
alias f='find . -maxdepth 1 | sed "s|^\./||" | fzf'
alias lg="lazygit"


alias ls='exa'
alias l='exa'
alias ll='exa -l'
alias la='exa -la'
alias lt='exa -l --total-size' 
alias lat='exa -lA --total-size'
alias tree='exa --tree'
